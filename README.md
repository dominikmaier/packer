# Thesis Packer

Das Projekt erstellt mit Hilfe von Packer automatisch eine VM Vorlage im VMware vCenter. Die VM Vorlage wird im weiteren Verlauf von Ansible verwendet. 

### Verwendung
1. mv variables.json.example variables.json
2. *variables.json* entsprechend konfigurieren
3. Deployment starten mit "packer build -var-file variables.json ubuntu.json "

### Quellen
https://www.packer.io/docs/
https://learn.hashicorp.com/tutorials/packer/getting-started-install?in=packer/getting-started
https://help.ubuntu.com/lts/installation-guide/s390x/apb.html
https://help.ubuntu.com/lts/installation-guide/example-preseed.txt
Cloud-Infrastrukturen: Infrastructure as a Service – So geht moderne IT-Infrastruktur. Das Handbuch für DevOps-Teams und Administratoren  

